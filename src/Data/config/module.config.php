<?php
return [
    'service_manager' => [
        'factories' => [
            \Data\V1\Rest\Products\ProductsResource::class => \Data\V1\Rest\Products\ProductsResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'data.rest.products' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/dataproducts',
                    'defaults' => [
                        'controller' => 'Data\\V1\\Rest\\Products\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'data.rest.products',
        ],
    ],
    'zf-rest' => [
        'Data\\V1\\Rest\\Products\\Controller' => [
            'listener' => \Data\V1\Rest\Products\ProductsResource::class,
            'route_name' => 'data.rest.products',
            'route_identifier_name' => 'products_id',
            'collection_name' => 'products',
            'entity_http_methods' => [
                0 => 'GET',
            ],
            'collection_http_methods' => [
                0 => 'GET',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \Data\V1\Rest\Products\ProductsEntity::class,
            'collection_class' => \Data\V1\Rest\Products\ProductsCollection::class,
            'service_name' => 'products',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'Data\\V1\\Rest\\Products\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'Data\\V1\\Rest\\Products\\Controller' => [
                0 => 'application/vnd.data.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'Data\\V1\\Rest\\Products\\Controller' => [
                0 => 'application/vnd.data.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \Data\V1\Rest\Products\ProductsEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'data.rest.products',
                'route_identifier_name' => 'products_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \Data\V1\Rest\Products\ProductsCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'data.rest.products',
                'route_identifier_name' => 'products_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-mvc-auth' => [
        'authorization' => [
            'Data\\V1\\Rest\\Products\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
            ],
        ],
    ],
];
